$StackName = "log-life"

Write-Output "Updating stack $StackName"
$StackUpdateOutput = (aws cloudformation update-stack --stack-name $StackName --template-body file://$PWD/template.yaml --capabilities CAPABILITY_AUTO_EXPAND CAPABILITY_IAM) | ConvertFrom-Json
$StackId = $StackUpdateOutput.StackId

Write-Output "Waiting stack $StackId status"
aws cloudformation wait stack-update-complete --stack-name $StackName

$StackInfo = (aws cloudformation describe-stacks --stack-name $StackName) | ConvertFrom-Json
Write-Output $StackInfo.Stacks[0].Outputs