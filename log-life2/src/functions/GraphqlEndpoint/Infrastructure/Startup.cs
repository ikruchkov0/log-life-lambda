using System;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.Lambda.Core;
using GraphQL;
using GraphqlEndpoint.Data.Repositories;
using GraphqlEndpoint.GraphQL;
using GraphqlEndpoint.Infrastructure.Configuration;
using GraphqlEndpoint.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace GraphqlEndpoint.Infrastructure
{
    public static class Startup
    {
        public static IServiceProvider GetServiceProvider()
        {
            var config = Configure(new ConfigurationBuilder()).Build();
            
            var serviceCollection = new ServiceCollection()
                .AddSingleton<IConfiguration>(config);
            
            return ConfigureServices(serviceCollection).BuildServiceProvider();
        }
        
        private static IConfigurationBuilder Configure(IConfigurationBuilder builder) => builder
            .AddEnvironmentVariables("LAMBDA_");
        
        private static IServiceCollection ConfigureServices(IServiceCollection services) => services
            .AddLogging(cfg => cfg.AddLambdaLogger(new LambdaLoggerOptions
            {
                IncludeNewline = true,
                IncludeLogLevel = true,
                IncludeScopes = true,
                IncludeCategory = true,
                IncludeEventId = false,
                IncludeException = true,
            }))
            .AddSingleton<FunctionConfiguration>()
            .AddSingleton<IAmazonDynamoDB>(sp =>
            {
                var config = sp.GetService<FunctionConfiguration>();
                var loggerFactory = sp.GetService<ILoggerFactory>();
                var logger = loggerFactory.CreateLogger(nameof(Startup));
                if (string.IsNullOrEmpty(config.LocalDynamoDbUrl))
                {
                    logger.LogInformation("Global dynamo db client");
                    return new AmazonDynamoDBClient();
                }

                var clientConfig = new AmazonDynamoDBConfig
                {
                    ServiceURL = config.LocalDynamoDbUrl
                };
                logger.LogInformation("Local dynamo db client");
                return new AmazonDynamoDBClient(clientConfig);
            })
            .AddSingleton<IDynamoDBContext, DynamoDBContext>()
            .AddSingleton<ReceiptRepository>()
            .AddReceiptGraphQL()
            .AddSingleton<GraphQLRequestProcessor>();
    }
}