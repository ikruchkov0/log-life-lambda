using LogLife.Generator.Helpers;
using LogLife.Generator.ViewModels;
using RazorLight;

namespace LogLife.Generator.Render
{
    public class OperationRenderContext : RenderContext
    {
        private readonly Operation _operation;

        public OperationRenderContext(RenderSettings settings, IRazorLightEngine engine, Operation operation) : base(settings, engine)
        {
            _operation = operation;
        }

        public string MethodName => NameHelpers.Capitalize(_operation.OperationId);

        public string Route => _operation.Path;
        
        public string Method => NameHelpers.Capitalize(_operation.Method);
    }
}