using System.Collections.Generic;

namespace LogLife.Generator.ViewModels
{
    public class Model : IModel
    {
        public string Name { get; set; }

        public List<ModelProperty> Properties { get; set; }

        public Model()
        {
            Properties = new List<ModelProperty>();
        }
    }
}