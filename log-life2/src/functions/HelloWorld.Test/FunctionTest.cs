using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Xunit;
using Amazon.Lambda.TestUtilities;
using Amazon.Lambda.APIGatewayEvents;

namespace HelloWorld.Tests
{
    public class FunctionTest
    {
        [Fact]
        public async Task TestHelloWorldFunctionHandler()
        {
            System.Environment.SetEnvironmentVariable("AllowedOrigin", "123");
            var request = new APIGatewayProxyRequest
            {
                HttpMethod = "POST",
                Path = "/test"
            };
            var context = new TestLambdaContext();

            var expectedResponse = new APIGatewayProxyResponse
            {
                Body = $"[{request.HttpMethod}] {request.Path}",
                StatusCode = 200,
                Headers = new Dictionary<string, string>
                {
                    {"Content-Type", "application/json"},
                    { "Access-Control-Allow-Origin", System.Environment.GetEnvironmentVariable("AllowedOrigin") },
                }
            };

            var function = new Function();
            var response = await function.FunctionHandler(request, context);

            Console.WriteLine("Lambda Response: \n" + response.Body);
            Console.WriteLine("Expected Response: \n" + expectedResponse.Body);

            Assert.Equal(expectedResponse.Body, response.Body);
            Assert.Equal(expectedResponse.Headers, response.Headers);
            Assert.Equal(expectedResponse.StatusCode, response.StatusCode);
        }
    }
}