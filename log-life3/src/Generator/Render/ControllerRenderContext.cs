using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogLife.Generator.Helpers;
using LogLife.Generator.ViewModels;
using RazorLight;
using RazorLight.Text;

namespace LogLife.Generator.Render
{
    public class ControllerRenderContext: RenderContext
    {
        private readonly Tag _tag;

        public ControllerRenderContext(RenderSettings settings, IRazorLightEngine engine, Tag tag) : base(settings, engine)
        {
            _tag = tag;
        }

        public string ControllerName => NameHelpers.Capitalize(_tag.Name);

        public IEnumerable<OperationRenderContext> Operations => _tag
            .Operations
            .Select(o => new OperationRenderContext(Settings, Engine, o));
    }
}