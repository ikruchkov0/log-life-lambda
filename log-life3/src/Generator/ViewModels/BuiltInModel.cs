namespace LogLife.Generator.ViewModels
{
    public class BuiltInModel : IModel
    {
        public string Name { get; }
        
        public BuiltInModel(string type)
        {
            Name = type;
        }
    }
}