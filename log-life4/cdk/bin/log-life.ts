#!/usr/bin/env node
import 'source-map-support/register';
import cdk = require('@aws-cdk/core');
import { LogLifeStack } from '../lib/log-life-stack';

const app = new cdk.App();
new LogLifeStack(app, 'LogLifeStack');
