using System.Collections.Generic;

namespace LogLife.Generator.ViewModels
{
    public class Operation
    {
        public string Path { get; set; }

        public string Method { get; set; }

        public string OperationId { get; set; }

        public List<Response> Responses { get; set; }

        public Operation()
        {
            Responses = new List<Response>();
        }
    }
}