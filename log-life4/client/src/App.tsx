import * as React from 'react';
import './App.css';

interface IState {
  data: string;
}

const apiUrl = 'http://localhost:3000';

const getData = async () => {
  const res = await fetch(apiUrl);
  const json = await res.json();
  return json;
};

const sendData = async (data: {}) => {
  const res = await fetch(apiUrl, {
    body: JSON.stringify(data),
    method: 'POST',
  });
  const json = await res.json();
  return json;
};

class App extends React.Component<{}, IState> {

  constructor(props: {}) {
    super(props);
    this.state = {
      data: 'No Data'
    };
    this.add= this.add.bind(this);
  }

  public componentDidMount() {
    this.setState({
      ...this.state,
      data: 'Loading...',
    });

    getData()
    .then(j => this.setState({
      ...this.state,
      data: JSON.stringify(j),
    }))
    .catch(e => this.setState({
      ...this.state,
      data: JSON.stringify(e),
    }));
  }

  public render() {
    return (
      <div className="App">
        <p>
          {this.state.data}
        </p>
        <button onClick={this.add} >Add</button>
      </div>
    );
  }

  private async add() {
    this.setState({
      ...this.state,
      data: 'Sending...',
    });

    sendData({
      name: 'abc',
      uploadedByUserId: 1,
    })
    .then(j => this.setState({
      ...this.state,
      data: JSON.stringify(j),
    }))
    .catch(e => this.setState({
      ...this.state,
      data: JSON.stringify(e),
    }));
  }
}

export default App;
