using Microsoft.Extensions.Configuration;

namespace GraphqlEndpoint.Infrastructure.Configuration
{
    public class FunctionConfiguration
    {
        public string AllowedOrigin { get; }
        
        public string LocalDynamoDbUrl { get; }

        public FunctionConfiguration(IConfiguration configuration)
        {
            AllowedOrigin = configuration.GetValue<string>("AllowedOrigin");
            LocalDynamoDbUrl = configuration.GetValue<string>("LocalDynamoDBUrl");
        }
    }
}