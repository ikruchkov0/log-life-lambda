import envFile from '../env.json';
import { get, getText, post } from './http';

interface EndpointConfig {
    OutputKey: string;
    OutputValue: string;
    Description: string;
}

type EnvConfig = Array<Array<EndpointConfig>>;

const envConfig = envFile as EnvConfig;

const baseUrl = 'http://127.0.0.1:3000';
    
const getServiceEndpoint = (service: string) => {
    if (!envConfig)
    {
        return '';
    }

    if (envConfig.length === 0)
    {
        return '';
    }

    const endpoint = envConfig[0].find(x => x.OutputKey === service);
    
    if (!endpoint) {
        return '';
    }

    return endpoint.OutputValue;
};

export const hello = () => getText(baseUrl + '/hello');
export const native = () => post<{}, { message: string }>(baseUrl + '/graphql-native', {});