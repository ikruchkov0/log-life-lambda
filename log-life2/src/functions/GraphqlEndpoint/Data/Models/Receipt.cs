﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;

namespace GraphqlEndpoint.Data.Models
{
    [DynamoDBTable(nameof(Receipt))]
    public class Receipt
    {
        [DynamoDBHashKey] 
        public string Id { get; set; }

        [DynamoDBProperty] 
        public string UploadedByUserId { get; set; }

        [DynamoDBProperty] 
        public decimal Total { get; set; }


        public static async Task EnsureTable(IAmazonDynamoDB client)
        {
            var tableResponse = await client.ListTablesAsync();
            if (!tableResponse.TableNames.Contains(nameof(Receipt)))
            {
                await client.CreateTableAsync(new CreateTableRequest
                {
                    TableName = nameof(Receipt),
                    ProvisionedThroughput = new ProvisionedThroughput
                    {
                        ReadCapacityUnits = 3,
                        WriteCapacityUnits = 1
                    },
                    KeySchema = new List<KeySchemaElement>
                    {
                        new KeySchemaElement
                        {
                            AttributeName = nameof(Id),
                            KeyType = KeyType.HASH
                        }
                    },
                    AttributeDefinitions = new List<AttributeDefinition>
                    {
                        new AttributeDefinition
                        {
                            AttributeName = nameof(Id),
                            AttributeType = ScalarAttributeType.S
                        },
                        new AttributeDefinition
                        {
                            AttributeName = nameof(UploadedByUserId),
                            AttributeType = ScalarAttributeType.S
                        }
                    },
                    GlobalSecondaryIndexes = new List<GlobalSecondaryIndex>
                    {
                        new GlobalSecondaryIndex
                        {
                            IndexName = nameof(UploadedByUserId),
                            KeySchema = new List<KeySchemaElement>
                            {
                                new KeySchemaElement
                                {
                                    AttributeName = nameof(UploadedByUserId),
                                    KeyType = KeyType.HASH
                                }
                            },
                            Projection = new Projection
                            {
                                ProjectionType = ProjectionType.KEYS_ONLY
                            },
                            ProvisionedThroughput = new ProvisionedThroughput
                            {
                                ReadCapacityUnits = 3,
                                WriteCapacityUnits = 1
                            },
                        },
                    }
                });
                TableStatus status;
                do
                {
                    var description = await client.DescribeTableAsync(nameof(Receipt));
                    status = description.Table.TableStatus;
                } while (status != TableStatus.ACTIVE);
            }
        }
    }
}