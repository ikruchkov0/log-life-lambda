import React from 'react';
import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';

import './App.css';

import { hello, native } from './services/v1';

interface State {
  helloWorldText?: string;
  loading?: boolean;
  graphQLData?: any;
}

interface Receipt {
  id: string;
  uploadedByUserId: string;
  total: number;
}

interface ReceiptVars {
  id: string;
}

const client = new ApolloClient({
  uri: 'http://localhost:3000/v1/graphql'
  //uri: 'https://xr5vzjvoxk.execute-api.eu-central-1.amazonaws.com/v1/graphql/'
});

export class App extends React.Component<{}, State> {

  constructor(props: {}) {
    super(props);
    this.state = {};
  }

  render() {

    const { helloWorldText, loading, graphQLData } = this.state;

    return (
      <div className="App">
        {helloWorldText}
        {loading ? <p>Loading ...</p> : helloWorldText ? helloWorldText : 'No data' }
      </div>
    );
  }

  componentDidMount() {
    this.setState({
      loading: true,
      ...this.state, 
    });

    native().then(r => this.setState({
      helloWorldText: r.message,
      loading: false,
      ...this.state, 
    })).catch(e => this.setState({
      helloWorldText: e,
      loading: false,
      ...this.state, 
    }));

    

    /*client.query<Receipt>({
      query: gql`
        query Receipt {
          receipt(id: "1") {
            id,
            total
          }
        }
      `,
    })
      .then(data => this.setState({
        loading: false,
        graphQLData: data,
        ...this.state, 
      }))
      .catch(error => console.error(error));*/
  }
}

export default App;
