$ContractFile="contract.yml"
$ClassName="Contract"
$Namespace="LogLife.Web.Controller.Generated"
$Output="Controllers/Generated/ResourceController.cs"
$UseLiquidTemplates="true"
$AspNetNamespace="""Microsoft.AspNetCore.Mvc"""
$ControllerBaseClass="""Microsoft.AspNetCore.Mvc.Controller"""
$ControllerStyle="abstract"
$TemplateDirectory="./templates/CSharp"


dotnet restore
dotnet nswag openapi2cscontroller `
/input:$ContractFile `
/classname:$ClassName `
/namespace:$Namespace `
/output:$Output `
/UseLiquidTemplates:$UseLiquidTemplates `
/AspNetNamespace:$AspNetNamespace `
/ControllerBaseClass:$ControllerBaseClass `
/ControllerStyle:$ControllerStyle `
/TemplateDirectory:$TemplateDirectory

$Output="wwwroot/Client/Generated/Client.ts"
$TemplateDirectory="./templates/TypeScript"

dotnet nswag openapi2tsclient `
/input:$ContractFile `
/classname:$ClassName `
/output:$Output `
/UseLiquidTemplates:$UseLiquidTemplates `
/TemplateDirectory:$TemplateDirectory