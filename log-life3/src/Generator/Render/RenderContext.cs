using RazorLight;

namespace LogLife.Generator.Render
{
    public abstract class RenderContext
    {
        protected RenderContext(RenderSettings settings, IRazorLightEngine engine)
        {
            Engine = engine;
            Settings = settings;
        }

        public string Namespace => Settings.Namespace;

        protected RenderSettings Settings { get; }
        
        protected IRazorLightEngine Engine { get;  }
    }
}