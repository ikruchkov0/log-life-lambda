using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json;
using Xunit;
using Amazon.Lambda.TestUtilities;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using GraphqlEndpoint.Data;
using GraphqlEndpoint.Data.Models;
using GraphqlEndpoint.ViewModels;
using Xunit.Abstractions;

namespace GraphqlEndpoint.Tests
{
    public class TestLogger : ILambdaLogger
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public TestLogger(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        #region Implementation of ILambdaLogger

        public void Log(string message)
        {
            _testOutputHelper.WriteLine(message);
        }

        public void LogLine(string message)
        {
            _testOutputHelper.WriteLine(message);
        }

        #endregion
    }
    
    public static class TestConstants
    {
        public const string LocalDynamoDBUrl = "http://localhost:8000";
    }
    
    public class TestData
    {
        private readonly string _dynamoDbUrl;

        public TestData(string dynamoDbUrl)
        {
            _dynamoDbUrl = dynamoDbUrl;
        }
        
        public async Task Populate()
        {
            var config = new AmazonDynamoDBConfig
            {
                ServiceURL = _dynamoDbUrl
            };
            var client = new AmazonDynamoDBClient(config);

            await Receipt.EnsureTable(client);
        }
    }
    
    public class FunctionTest
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public FunctionTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        private const string Query = @"
{
    receipts(userId: ""1"") {
        id,
        total
    }
}";
        
        [Fact]
        public async Task TestGraphqlEndpointFunctionHandler()
        {
            const string allowedOrigin = "123";
            Environment.SetEnvironmentVariable("LAMBDA_AllowedOrigin", allowedOrigin);
            Environment.SetEnvironmentVariable("LAMBDA_LocalDynamoDBUrl", TestConstants.LocalDynamoDBUrl);
            
            var testData = new TestData(TestConstants.LocalDynamoDBUrl);
            await testData.Populate();
            
            var request = new APIGatewayProxyRequest
            {
                HttpMethod = "POST",
                Body = JsonConvert.SerializeObject(new GraphQLRequest
                {
                    Query = Query
                })
            };
            var context = new TestLambdaContext
            {
                Logger = new TestLogger(_testOutputHelper)
            };

            var expectedResponse = new APIGatewayProxyResponse
            {
                Body = "",
                StatusCode = 200,
                Headers = new Dictionary<string, string>
                {
                    {"Content-Type", "application/json"},
                    { "Access-Control-Allow-Origin", allowedOrigin },
                }
            };
            
            var function = new Function();
            var response = await function.FunctionHandler(request, context);
            
            _testOutputHelper.WriteLine(response.Body);
            
            //Assert.Equal(expectedResponse.Body, response.Body);
            Assert.Equal(expectedResponse.Headers, response.Headers);
            Assert.Equal(expectedResponse.StatusCode, response.StatusCode);
        }
    }
}