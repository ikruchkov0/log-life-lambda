using GraphQL;
using GraphQL.Types;

namespace GraphqlEndpoint.GraphQL
{
    public class ReceiptSchema : Schema
    {
        public ReceiptSchema(IDependencyResolver resolver)
        {
            Query = resolver.Resolve<ReceiptQuery>();
        }
    }
}