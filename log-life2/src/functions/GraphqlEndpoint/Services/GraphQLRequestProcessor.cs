using System;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Instrumentation;
using GraphQL.Types;
using GraphqlEndpoint.ViewModels;
using Microsoft.Extensions.Logging;

namespace GraphqlEndpoint.Services
{
    public class GraphQLRequestProcessor
    {
        private readonly ISchema _schema;
        private readonly ILogger _logger;
        private readonly IDocumentExecuter _documentExecuter;

        public GraphQLRequestProcessor(
            ILogger<GraphQLRequestProcessor> logger, ISchema schema, IDocumentExecuter documentExecuter)
        {
            _logger = logger;
            _schema = schema;
            _documentExecuter = documentExecuter;
        }
        
        public async Task<ExecutionResult> ProcessRequestAsync(GraphQLRequest request)
        {
            var start = DateTime.UtcNow;
            _logger.LogDebug("Start query {query} execution", request.Query);
            
            var inputs = request.Variables.ToInputs();
            
            var result = await _documentExecuter.ExecuteAsync(_ =>
            {
                _.Schema = _schema;
                _.Query = request.Query;
                _.OperationName = request.OperationName;
                _.Inputs = inputs;
                _.EnableMetrics = true;
                _.FieldMiddleware.Use<InstrumentFieldsMiddleware>();
            });
            
            result.EnrichWithApolloTracing(start);
            
            return result;
        }
    }
}