using System.Collections.Generic;
using System.Linq;
using Microsoft.OpenApi.Any;

namespace LogLife.Generator.ViewModels
{
    public class Enum : IModel
    {
        public string Name { get; }

        public List<EnumCase> Cases { get; }

        public Enum(string name, IList<IOpenApiAny> cases)
        {
            Name = name;
            Cases = cases.Select(EnumCase.From).ToList();
        }
    }
}