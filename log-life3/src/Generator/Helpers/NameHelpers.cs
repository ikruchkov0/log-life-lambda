namespace LogLife.Generator.Helpers
{
    public static class NameHelpers
    {
        public static string Capitalize(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }

            return $"{str.Substring(0, 1).ToUpper()}{str.Substring(1)}";
        }
    }
}