import { APIGatewayProxyEvent, APIGatewayProxyResult, Context } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';

const isLocal = process.env.AWS_SAM_LOCAL;
const TABLE_NAME = process.env.TABLE_NAME || '';

const dynamoDb = isLocal === 'true' ? new DynamoDB.DocumentClient({
  endpoint: 'http://dynamodb:8000'
}) : new DynamoDB.DocumentClient();

export const main = async (event: APIGatewayProxyEvent, context: Context): Promise<APIGatewayProxyResult> => {
  try {
    switch (event.httpMethod) {
      case 'POST':
        return add(event.body);
      default:
        return getAll();
    }
  } catch (error) {
    var body = error.stack || JSON.stringify(error, null, 2);
    return serverError(JSON.stringify(body));
  }
};

const response = (body: string, statusCode: number): APIGatewayProxyResult => ({
  statusCode,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': '*',
    'Access-Control-Allow-Headers': '*',
    'Content-Type': 'application/json'
  },
  body
});

const getAll = async () => {
  const params = {
    TableName: TABLE_NAME
  };
  const response = await dynamoDb.scan(params).promise();
  return ok(JSON.stringify(response.Items));
};

const add = async (body: string | null) => {
  if (!body) {
    return badRequest('Empty request body');
  }

  const { name, uploadedByUserId } = JSON.parse(body);
  const params = {
    TableName: TABLE_NAME,
    Item: {
      id : Date.now().toString(),
      name,
      uploadedByUserId
    }
  };

  const r = await dynamoDb.put(params).promise();
  if (r.$response.error) {
    return serverError(JSON.stringify(r.$response.error));
  }
  return ok(JSON.stringify(r.$response.data));
}

const ok = (body: string) => response(body, 200);

const serverError = (body: string) => response(body, 500);

const badRequest = (body: string) => response(body, 400);