using GraphQL.Types;
using GraphqlEndpoint.Data.Repositories;

namespace GraphqlEndpoint.GraphQL
{
    public class ReceiptQuery : ObjectGraphType
    {
        public ReceiptQuery(ReceiptRepository repository)
        {
            FieldAsync<ReceiptType>(
                "Receipt",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType>
                    {
                        Name = "id", Description = "The Id of Receipt."
                    }
                ),
                resolve: async context => await repository.GetById(context.GetArgument<string>("id")) );

            FieldAsync<ListGraphType<ReceiptType>>(
                "Receipts",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType>
                    {
                        Name = "userId", Description = "User who uploaded receipts."
                    }
                ),
                resolve: async context => await repository.GetByUserId(context.GetArgument<string>("userId")));
        }
    }
}