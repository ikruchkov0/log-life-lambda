using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Amazon.Lambda.Core;
using Amazon.Lambda.APIGatewayEvents;
using GraphqlEndpoint.Infrastructure;
using GraphqlEndpoint.Infrastructure.Configuration;
using GraphqlEndpoint.Services;
using GraphqlEndpoint.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;



// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace GraphqlEndpoint
{
    public class Function
    {
        private readonly FunctionConfiguration _configuration;
        private readonly GraphQLRequestProcessor _requestProcessor;
        private readonly ILogger _logger;
        

        public Function()
        {
            var serviceProvider = Startup.GetServiceProvider();
            _logger = serviceProvider.GetService<ILogger<Function>>();
            _configuration = serviceProvider.GetService<FunctionConfiguration>();
            _requestProcessor = serviceProvider.GetService<GraphQLRequestProcessor>();
        }

        public async Task<APIGatewayProxyResponse> FunctionHandler(
            APIGatewayProxyRequest apiGatewayProxyRequest,
            ILambdaContext context)
        {
            _logger.LogInformation("Start handling request {request}", apiGatewayProxyRequest.Body);
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var request = JsonConvert.DeserializeObject<GraphQLRequest>(apiGatewayProxyRequest.Body);
            
            var result = await _requestProcessor.ProcessRequestAsync(request);
            stopWatch.Stop();
            _logger.LogInformation("End handling request {request} {elapsed}", apiGatewayProxyRequest.Body, stopWatch.Elapsed);
            
            if (result.Errors?.Count > 0)
            {
                return CreateResponse(HttpStatusCode.BadRequest, result.Errors);
            }

            return CreateResponse(HttpStatusCode.OK, result);
        }
        
        private APIGatewayProxyResponse CreateResponse(HttpStatusCode statusCode, object result) =>
            new APIGatewayProxyResponse
            {
                Body = JsonConvert.SerializeObject(result),
                StatusCode = (int) statusCode,
                Headers = new Dictionary<string, string>
                {
                    {"Content-Type", "application/json"},
                    {"Access-Control-Allow-Origin", _configuration.AllowedOrigin},
                }
            };

        
    }
}