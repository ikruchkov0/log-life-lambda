﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Threading.Tasks;
using LogLife.Generator.Helpers;
using LogLife.Generator.Render;
using LogLife.Generator.ViewModels;
using Microsoft.OpenApi.Extensions;
using Microsoft.OpenApi.Models;
using Microsoft.OpenApi.Readers;
using RazorLight;
using RazorLight.Compilation;
using RazorLight.Razor;
using Scriban;
using Enum = LogLife.Generator.ViewModels.Enum;


namespace LogLife.Generator
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var contract = await System.IO.File.ReadAllTextAsync("contract.yml");
            var schema = new OpenApiStringReader().Read(contract, out var diagnostic);

            var tagsMap = new Dictionary<string, Tag>();
            var modelsMap = new Dictionary<string, Model>();
            
            Func<string, Model> getModel = (modelRef) =>
            {
                if (!modelsMap.TryGetValue(modelRef, out var modelDto))
                {
                    throw new Exception($"Unable to find reference {modelRef}");
                }

                return modelDto;
            };
            
            Func<OpenApiSchema, Model> getModelBySchema = s =>
            {
                if (s.Reference == null)
                {
                    throw new Exception("Only references responses supported");
                }

                return getModel(s.Reference.Id);
            };

            
            // add model definitions
            foreach (var kvModel in schema.Components.Schemas)
            {
                var modelDto = new Model
                {
                    Name = kvModel.Key,
                };

                modelsMap[modelDto.Name] = modelDto;
            }
            
            foreach (var kvModel in schema.Components.Schemas)
            {
                var modelDto = getModel(kvModel.Key);
                
                foreach (var kvProp in kvModel.Value.Properties)
                {
                    IModel propModelDto;
                    var type = kvProp.Value;
                    switch (type.Type)
                    {
                        case "array":
                            propModelDto = new ArrayType(getModelBySchema(type.Items));
                            break;
                        case "string":
                        case "integer":
                            if (type.Enum.Count == 0)
                            {
                                propModelDto = new BuiltInModel(type.Type);
                                break;
                            }

                            propModelDto = new Enum($"{modelDto.Name}{kvProp.Key}", type.Enum);
                            break;
                        default:
                            propModelDto = getModelBySchema(type);
                            break;
                    }

                    var prop = new ModelProperty
                    {
                        Name = kvProp.Key,
                        Model = propModelDto,
                    };
                    
                    modelDto.Properties.Add(prop);
                }
            }
            
            
            foreach (var kvPath in schema.Paths)
            {
                var operations = kvPath.Value.Operations;
                foreach (var kvOperation in operations)
                {
                    var operation = kvOperation.Value;
                    var operationDto = new Operation
                    {
                        Path = kvPath.Key,
                        OperationId = operation.OperationId,
                        Method = kvOperation.Key.GetDisplayName(),
                    };
                    
                    foreach (var tag in operation.Tags)
                    {
                        if (!tagsMap.TryGetValue(tag.Name, out var tagDto))
                        {
                            tagDto = new Tag
                            {
                                Name = tag.Name,
                            };
                            tagsMap[tag.Name] = tagDto;
                        }
                        
                        tagDto.Operations.Add(operationDto);
                    }

                    if (operation.RequestBody?.Content != null)
                    {
                        foreach (var mediaType in operation.RequestBody.Content)
                        {
                            //Console.WriteLine(mediaType.Key);
                        }
                    }

                    foreach (var response in operation.Responses)
                    {
                        foreach (var mediaType in response.Value.Content)
                        {
                            if (mediaType.Value.Schema.Reference == null)
                            {
                                throw new Exception("Only references responses supported");
                            }
                            
                            var modelDto = getModel(mediaType.Value.Schema.Reference.Id);
                            
                            var responseDto = new Response
                            {
                                Code = response.Key,
                                MediaType = mediaType.Key,
                                Model =  modelDto,
                            };
                            operationDto.Responses.Add(responseDto);
                        }
                        
                    }
                }
            }
            
            var absolutePath = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
            var templatesPath = Path.Join(absolutePath, "../../../Templates");
            var outputPath = Path.Join(absolutePath, "../../../Output");
            if (!Directory.Exists(outputPath))
            {
                Directory.CreateDirectory(outputPath);
            }

            var engine = new RazorLightEngineBuilder()
                .UseProject(new FileSystemRazorProject(templatesPath))
                .UseMemoryCachingProvider()
                .Build();

            
            var settings = new RenderSettings
            {
                Namespace = "LogLife.Output.Controllers",
            };

            var templateStr = await File.ReadAllTextAsync(Path.Join(templatesPath, "Controller.cs.template"));
            var template = Template.Parse(templateStr);
            
            try
            {
                foreach (var tag in tagsMap.Values)
                {
                    var controllerContext = new ControllerRenderContext(settings, engine, tag);
                    var result = template.Render(controllerContext);
                    await File.WriteAllTextAsync(Path.Join(outputPath, $"{NameHelpers.Capitalize(tag.Name)}Controller.cs"), result);
                
                }
            }
            catch (TemplateCompilationException ex)
            {
                Console.WriteLine(string.Join("\n", ex.CompilationErrors));
            }
            
        }
    }
};