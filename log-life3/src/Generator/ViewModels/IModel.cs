namespace LogLife.Generator.ViewModels
{
    public interface IModel
    {
        string Name { get; }
    }
}