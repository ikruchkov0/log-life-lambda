namespace LogLife.Generator.ViewModels
{
    public class ArrayType : IModel
    {
        public IModel Type { get; }
        
        public string Name { get; }
        
        public ArrayType(IModel type)
        {
            Type = type;
            Name = type.Name;
        }
    }
}