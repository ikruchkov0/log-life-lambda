﻿using GraphQL.Types;
using GraphqlEndpoint.Data;
using GraphqlEndpoint.Data.Models;

namespace GraphqlEndpoint.GraphQL
{
    public class ReceiptType : ObjectGraphType<Receipt>
    {
        public ReceiptType()
        {
            Name = "Receipt";

            Field(x => x.Id, type: typeof(IdGraphType)).Description("The Id of Receipt.");
            Field(x => x.Total).Description("Total sum in the receipt");
            Field(x => x.UploadedByUserId).Description("UserId of user who uploaded this receipt");
        }
    }
}