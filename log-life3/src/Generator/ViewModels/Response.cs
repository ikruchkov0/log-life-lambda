namespace LogLife.Generator.ViewModels
{
    public class Response
    {
        public string Code { get; set; }

        public string MediaType { get; set; }

        public Model Model { get; set; }
        
    }
}