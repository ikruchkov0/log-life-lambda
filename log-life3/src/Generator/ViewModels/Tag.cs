using System.Collections.Generic;

namespace LogLife.Generator.ViewModels
{
    public class Tag
    {
        public string Name { get; set; }

        public List<Operation> Operations { get; set; }

        public Tag()
        {
            Operations = new List<Operation>();
        }
    }
}