package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/graphql-go/graphql"
)

type GraphQLRequest struct {
	Query string `json:"query"`
}

type GraphQLResponse struct {
	Data *graphql.Result `json:"data"`
}

var schema *graphql.Schema

type Receipt struct {
	Id               string  `json:"id"`
	Total            float32 `json:"total"`
	UploadedByUserId string  `json:"uploadedByUserId"`
}

func init() {

	receiptType := graphql.NewObject(graphql.ObjectConfig{
		Name: "Receipt",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.ID,
			},
			"total": &graphql.Field{
				Type: graphql.Float,
			},
			"uploadedByUserId": &graphql.Field{
				Type: graphql.String,
			},
		},
	})

	rootQuery := graphql.ObjectConfig{
		Name: "Root",
		Fields: graphql.Fields{
			"receipt": &graphql.Field{
				Type: receiptType,
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					idStr, found := p.Args["id"]
					if !found {
						return nil, fmt.Errorf("Unable to find id parama")
					}
					id, ok := idStr.(string)
					if !ok {
						return nil, fmt.Errorf("Unable to cast id to string")
					}
					return &Receipt{
						Id:               id,
						Total:            123.0,
						UploadedByUserId: "123",
					}, nil
				},
			},
		},
	}

	schemaConfig := graphql.SchemaConfig{Query: graphql.NewObject(rootQuery)}
	s, err := graphql.NewSchema(schemaConfig)
	if err != nil {
		log.Fatalf("failed to create new schema, error: %v", err)
	}
	schema = &s
}

func HandleLambdaEvent(event events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	fmt.Printf("Processing graphql request %s\n", event.Body)

	headers := map[string]string{
		"Content-Type":                "application/json",
		"Access-Control-Allow-Origin": "*",
	}

	request := GraphQLRequest{}

	err := json.Unmarshal([]byte(event.Body), &request)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 500}, nil
	}

	params := graphql.Params{Schema: *schema, RequestString: request.Query}
	r := graphql.Do(params)
	if len(r.Errors) > 0 {
		fmt.Printf("failed to execute graphql operation, errors: %+v\n", r.Errors)
		return events.APIGatewayProxyResponse{
			Body:       "graphql error",
			StatusCode: 400,
			Headers:    headers}, nil
	}

	response := GraphQLResponse{
		Data: r,
	}

	responseJSON, err := json.Marshal(&response)
	if err != nil {
		return events.APIGatewayProxyResponse{
			Body:       err.Error(),
			StatusCode: 500,
			Headers:    headers,
		}, nil
	}

	return events.APIGatewayProxyResponse{
		Body:       string(responseJSON),
		StatusCode: 200,
		Headers:    headers,
	}, nil
}

func main() {
	lambda.Start(HandleLambdaEvent)
}
