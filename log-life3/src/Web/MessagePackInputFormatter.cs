using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using LogLife.Web.Controller.Generated;
using MessagePack;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace LogLife.Web
{
    public class MessagePackInputFormatter : IInputFormatter
    {
        delegate Task<object> Deserializer(Stream stream);

        private readonly Dictionary<Type, Deserializer> _deserializers;

        public MessagePackInputFormatter()
        {
            _deserializers = new Dictionary<Type, Deserializer>
            {
                {typeof(Hello), async s => await MessagePackSerializer.DeserializeAsync<Hello>(s)},
                {typeof(Hello2), async s => await MessagePackSerializer.DeserializeAsync<Hello2>(s)}
            };
        }

        #region Implementation of IInputFormatter

        public bool CanRead(InputFormatterContext context)
        {
            return _deserializers.ContainsKey(context.ModelType);
        }

        public async Task<InputFormatterResult> ReadAsync(InputFormatterContext context)
        {
            var resultObj = await _deserializers[context.ModelType](context.HttpContext.Request.Body);
            
            return InputFormatterResult.Success(resultObj);
        }

        #endregion
    }
}