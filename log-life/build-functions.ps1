$ArtifactPath = "$PWD/build/functions"
$FunctionName = "TestFunction"
Start-Process -FilePath "dotnet" -ArgumentList "lambda package -o $ArtifactPath/$FunctionName.zip" -NoNewWindow -Wait -WorkingDirectory "$PWD/functions/$FunctionName/src/$FunctionName"