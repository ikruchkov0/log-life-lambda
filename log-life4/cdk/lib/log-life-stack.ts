import * as fs from 'fs';
import * as path from 'path';

import * as core from '@aws-cdk/core';
import * as s3 from '@aws-cdk/aws-s3';
import * as s3deployment from '@aws-cdk/aws-s3-deployment';
import * as lambda from '@aws-cdk/aws-lambda';
import * as apigateway from '@aws-cdk/aws-apigateway';
import * as dynamodb from '@aws-cdk/aws-dynamodb';

const clientAssets = '../client/build';

export class LogLifeStack extends core.Stack {
  constructor(scope: core.App, id: string, props?: core.StackProps) {
    super(scope, id, props);

    const spaBucket = new s3.Bucket(this, 'SPABucket', {
      versioned: false,
      bucketName: `${id.toLowerCase()}-spa`,
      publicReadAccess: true,
      websiteIndexDocument: 'index.html',
      websiteErrorDocument: 'index.html'
    });

    new s3deployment.BucketDeployment(this, 'DeploySPA', {
      sources: [s3deployment.Source.asset(clientAssets)],
      destinationBucket: spaBucket,
    });

    const receiptTable = new dynamodb.Table(this, 'Receipt', {
      partitionKey: { name: 'id', type: dynamodb.AttributeType.STRING  },

      tableName: `${id.toLowerCase()}-Receipt`,
      readCapacity: 5,
      writeCapacity: 5,
    });

    receiptTable.addGlobalSecondaryIndex({
      indexName: 'uploadedByUserId',
      partitionKey: { name: 'uploadedByUserId', type: dynamodb.AttributeType.STRING  },
      readCapacity: 5,
      writeCapacity: 5
    });

    const graphqlEndpointFunction = new lambda.Function(this, "GraphqlEndpointFunction", {
      runtime: lambda.Runtime.NODEJS_10_X,
      functionName: 'GraphqlEndpointFunction',
      code: lambda.Code.asset('../build/functions'),
      handler: 'graphql-endpoint.main',
      memorySize: 128,
      environment: {
        TABLE_NAME: receiptTable.tableName,
      }
    });

    receiptTable.grantReadWriteData(graphqlEndpointFunction);

    const api = new apigateway.LambdaRestApi(this, 'api', {
      handler: graphqlEndpointFunction,
    });
  }
}