$BacketName = "log-life-artifacts"
$SPABacketName = "log-life-spa-content"
$StackName = "log-life"

#aws s3 rm s3://$BacketName --recursive

$ArtifactPath = "$PWD/build"
$FunctionsPath = "$PWD/src/functions"
$FunctionsArray = ("HelloWorld", "GraphqlEndpoint")

ForEach ($FunctionName in $FunctionsArray ) { 
    Start-Process -FilePath "dotnet" -ArgumentList "lambda package -o $ArtifactPath/$FunctionName.zip" -NoNewWindow -Wait -WorkingDirectory "$FunctionsPath/$FunctionName"
}

$env:GOOS = "linux"
Start-Process -FilePath "go"  -ArgumentList "build -o main main.go" -NoNewWindow -Wait -WorkingDirectory "$FunctionsPath/go-graphql"
$GOPATH = "$env:GOPATH"
Start-Process -FilePath "$GOPATH\bin\build-lambda-zip.exe"  -ArgumentList "-o $ArtifactPath/go-graphql.zip $FunctionsPath/go-graphql/main" -NoNewWindow -Wait 

Write-Output "Packaging..."
sam package --output-template-file packaged.yaml --s3-bucket $BacketName
Write-Output "Deploying..."
sam deploy  --template-file packaged.yaml --stack-name $StackName --capabilities CAPABILITY_IAM

aws cloudformation describe-stacks --stack-name $StackName --query 'Stacks[].Outputs' --output json > ./src/client/src/env.json

./publish-client.ps1

aws cloudformation describe-stacks --stack-name $StackName --query 'Stacks[].Outputs' --output table