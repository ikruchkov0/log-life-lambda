using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using LogLife.Web.Controller.Generated;
using MessagePack;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace LogLife.Web
{
    public class MessagePackOutputFormatter : IOutputFormatter
    {
        delegate Task Serializer(Stream stream, object obj);

        private readonly Dictionary<Type, Serializer> _serializers;

        public MessagePackOutputFormatter()
        {
            _serializers = new Dictionary<Type, Serializer>
            {
                {typeof(Hello), (stream, obj) => MessagePackSerializer.SerializeAsync(stream, (Hello)obj)},
                {typeof(Hello2), (stream, obj) => MessagePackSerializer.SerializeAsync(stream, (Hello2)obj)}
            };
        }
        
        #region Implementation of IOutputFormatter

        public bool CanWriteResult(OutputFormatterCanWriteContext context)
        {
            return _serializers.ContainsKey(context.ObjectType);
        }

        public Task WriteAsync(OutputFormatterWriteContext context)
        {
            context.ContentType = "text/plain";

            return _serializers[context.ObjectType](context.HttpContext.Response.Body, context.Object);
        }

        #endregion
    }
}