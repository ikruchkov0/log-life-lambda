$ArtifactPath = "$PWD/build/functions"
$FunctionName = "TestFunction"

aws s3api put-object --bucket log-life-lambda-artifacts --key $FunctionName --body $ArtifactPath/$FunctionName.zip