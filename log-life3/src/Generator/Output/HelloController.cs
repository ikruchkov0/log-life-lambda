using Microsoft.AspNetCore.Mvc;

namespace LogLife.Output.Controllers
{
    [ApiController]
    public class HelloController
    {
        
        [HttpGet]
        [Route("/v1/api/hello")]
        public IActionResult HelloRequest()
        {
        }
        
        [HttpPost]
        [Route("/v1/api/hello")]
        public IActionResult HelloResponse()
        {
        }
        
        [HttpGet]
        [Route("/v1/api/hello2/{id}")]
        public IActionResult Hello2Request()
        {
        }
        
        [HttpPost]
        [Route("/v1/api/hello2/{id}")]
        public IActionResult Hello2Response()
        {
        }
        
    }
}