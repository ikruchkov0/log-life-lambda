namespace LogLife.Generator.ViewModels
{
    public class ModelProperty
    {
        public string Name { get; set; }

        public IModel Model { get; set; }
    }
}