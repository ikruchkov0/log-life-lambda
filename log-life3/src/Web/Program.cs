using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.FastConsole;

namespace LogLife.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host
                .CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(builder =>
                {
                    builder
                        .ConfigureKestrel(opts =>
                        {
                            opts.Limits.MaxRequestBodySize = 100 * 1024;
                            opts.Limits.MaxResponseBufferSize = 100 * 1024;
                            opts.Limits.KeepAliveTimeout = TimeSpan.FromSeconds(1);
                            opts.AddServerHeader = false;
                            opts.Limits.MaxConcurrentConnections = 10;
                        })
                        .UseStartup<Startup>()
                        .UseSerilog(ConfigureSerilog);
                });

        private static void ConfigureSerilog(WebHostBuilderContext context, LoggerConfiguration loggerConfiguration) =>
            loggerConfiguration
                .Enrich.FromLogContext()
                .Enrich.WithProperty("Environment", context.HostingEnvironment.EnvironmentName)
                .Enrich.WithProperty("Application", context.HostingEnvironment.ApplicationName)
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                .WriteTo.FastConsole();

    }
}
