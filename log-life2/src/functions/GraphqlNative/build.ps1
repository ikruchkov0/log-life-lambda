docker build -t graphql-native-build .
Remove-Item –path out\* -Force
Remove-Item –path package.zip -Force
docker run -it --rm -v ${PWD}:/src -w /src graphql-native-build dotnet publish -v n -c Release -r linux-x64 -o /src/out
docker run -it --rm -v ${PWD}:/src -w /src/out graphql-native-build zip -r ../package.zip bootstrap
cp package.zip ../../../build/graphql-native.zip