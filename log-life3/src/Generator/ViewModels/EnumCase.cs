using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Extensions;

namespace LogLife.Generator.ViewModels
{
    public class EnumCase
    {
        public string Label { get;  }
        
        public string Value { get;  }
        
        public EnumCase(string label, string value)
        {
            Label = label;
            Value = value;
        }

        public static EnumCase From(IOpenApiAny anyType)
        {
            return new EnumCase(anyType.AnyType.GetDisplayName(), anyType.AnyType.GetDisplayName());
        }
    }
}