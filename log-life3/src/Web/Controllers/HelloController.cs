using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogLife.Web.Controller.Generated;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LogLife.Web.Controllers
{
    public class HelloController: ContractControllerBase
    {
        #region Overrides of ContractControllerBase

        public override Task<Hello> HelloRequest()
        {
            return Task.FromResult(new Hello
            {
                Message = "msg",
                TestEnum = HelloTestEnum._Case1
            });
        }
        
        public override Task<Hello> HelloResponse(Hello body)
        {
            return Task.FromResult(new Hello
            {
                Message = body.Message,
                TestEnum = body.TestEnum,
            });
        }

        public override Task<Hello2> Hello2Request(int id)
        {
            return Task.FromResult(new Hello2
            {
                Id = id,
                Items = Enumerable.Range(0, id).Select(i => new Hello { Message = $"_{i}"}).ToList()
            });
        }

        public override Task<Hello2> Hello2Response(int id, Hello2 body)
        {
            return Task.FromResult(new Hello2
            {
                Id = id + body.Id,
                Items = Enumerable.Range(0, id + body.Id).Select(i => new Hello { Message = $"_{i}"}).ToList()
            });
        }

        #endregion
    }
}