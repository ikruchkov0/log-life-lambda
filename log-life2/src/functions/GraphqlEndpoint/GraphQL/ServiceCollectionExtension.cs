using GraphQL;
using GraphQL.Http;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace GraphqlEndpoint.GraphQL
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddReceiptGraphQL(this IServiceCollection services)
        {
            return services
                .AddSingleton<IDependencyResolver>(s => new FuncDependencyResolver(s.GetRequiredService))
                .AddSingleton<IDocumentExecuter, DocumentExecuter>()
                .AddSingleton<IDocumentWriter, DocumentWriter>()
                .AddSingleton<ReceiptQuery>()
                .AddSingleton<ReceiptType>()
                .AddSingleton<ISchema, ReceiptSchema>();
        }
    }
}