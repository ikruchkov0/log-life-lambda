using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using LogLife.Web.Controller.Generated;
using MessagePack;
using MessagePack.Resolvers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace LogLife.Web
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services) =>
            services
                .AddLogging(lb => lb.AddSerilog(dispose: true))
                .AddMvcCore(opts =>
                {
                    opts.OutputFormatters.Clear();
                    opts.OutputFormatters.Add(new MessagePackOutputFormatter());
                    opts.InputFormatters.Clear();
                    opts.InputFormatters.Add(new MessagePackInputFormatter());
                }).ConfigureApplicationPartManager(apm =>
                {
                    
                    var assembly = typeof(Startup).GetTypeInfo().Assembly;
                    var part = new AssemblyPart(assembly);
                    apm.ApplicationParts.Clear();
                    apm.ApplicationParts.Add(part);
                    apm.FeatureProviders.Clear();
                    apm.FeatureProviders.Add(new ControllerFeatureProvider());
                });

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app
                .UseSerilogRequestLogging()
                .UseDefaultFiles()
                .UseStaticFiles()
                .UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
