const httRequest = <T>(method: string, url: string, data?: T) => fetch(url, {
  method: method,
  mode: 'no-cors',
  headers: {
    'Content-Type': 'application/json'
  },
  body: data ? JSON.stringify(data) : null,
})

const httRequestJSON = <T, TResponse>(method: string, url: string, data?: T) => httRequest(method, url, data)
  .then(r => r.json())
  .then(x => x as TResponse);

export const post = <T, TResponse>(url: string, data: T) => httRequestJSON<T, TResponse>('POST', url, data);

export const get = <T, TResponse>(url: string) => httRequestJSON<T, TResponse>('GET', url);

export const getText = <T>(url: string) => httRequest<T>('GET', url).then(r => r.text());