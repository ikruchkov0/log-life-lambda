using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using GraphqlEndpoint.Data.Models;
using Microsoft.Extensions.Logging;

namespace GraphqlEndpoint.Data.Repositories
{
    public class ReceiptRepository
    {
        private readonly IDynamoDBContext _dbContext;
        private readonly ILogger _logger;

        public ReceiptRepository(IDynamoDBContext dbContext, ILogger<ReceiptRepository> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task<Receipt> GetById(string id)
        {
            _logger.LogDebug("Start loading receipt by id {id}", id);

            var result = await _dbContext.LoadAsync<Receipt>(id);

            return result;
        }
        
        public async Task<IEnumerable<Receipt>> GetByUserId(string userId)
        {
            _logger.LogDebug("Start loading receipts by userId {userId}", userId);
            
            var result = new List<Receipt>();
            var search = _dbContext.ScanAsync<Receipt>(new[]
            {
                new ScanCondition(nameof(Receipt.UploadedByUserId), ScanOperator.Equal,userId)
            });
                        
            while (!search.IsDone)
            {
                var entities = await search.GetNextSetAsync();
                result.AddRange(entities);
            }
            
            return result;
        }
    }
}